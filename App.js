import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Button,
  Text,
  View,
  Alert,
  ActivityIndicator,
  TouchableOpacity,
  FlatList,
  DeviceEventEmitter,
  StatusBar
} from 'react-native';
import { List, ListItem } from 'react-native-elements'
import Dt157Module from './Dt157Module';


type Props = {};
export default class App extends Component<Props> {
   constructor(props) {
    super(props)
    this.state = {
      data:[],
      scanning: false,
      scanText: "Scan Devices",
      readValue: 'Value comes there...'
    }
  }


componentDidMount () {
  StatusBar.setHidden(true);
  DeviceEventEmitter.addListener('MESSAGE_BT_NOT_APPLICABLE', this.eventDeviceNotApplicable.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_BT_NOT_READY', this.eventBluetoothNotReady.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_SCAN_STARTED', this.eventScanStarted.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_SCAN_STOPPED', this.eventScanStopped.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_DEVICE', this.eventDeviceFound.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_DEVICE_DISCONNECTED', this.eventBtDeviceDisconnected.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_DATA', this.eventDataReceived.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_BT_DEVICE_NOT_FOUND', this.eventBtDeviceNotFound.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_BT_DEVICE_CONNECTED', this.eventBtDeviceConnected.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_BT_READY', this.eventBluetoothReady.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_LOCATION_NOT_READY', this.eventLocationNotReady.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_LOCATION_READY', this.eventLocationReady.bind(this))
  DeviceEventEmitter.addListener('MESSAGE_BT_DEVICE_CONNECTING', this.eventBtDeviceConnecting.bind(this))
}

eventDeviceNotApplicable(e){
  this.setState({readValue: 'Cihaz BLE desteklemiyor !'})
  this.setState({scanning: false});
  this.setState({scanText: "Scan Devices"});
}

eventBluetoothReady(e){
  this.setState({readValue: 'Bluetooth is ready'})
}

eventBluetoothNotReady(e){
  this.setState({readValue: 'Bluetooth is not ready'})
  this.setState({scanning: false});
  this.setState({scanText: "Scan Devices"});
}

eventLocationReady(e){
  this.setState({readValue: 'Location is ready'})
  this.setState({scanning: false});
  this.setState({scanText: "Scan Devices"});
}

eventLocationNotReady(e){
  this.setState({readValue: 'Location is not ready'})
  this.setState({scanning: false});
  this.setState({scanText: "Scan Devices"});
}

eventScanStarted(e){
  this.setState({readValue: 'Scanning...'})
}

eventScanStopped(e){
  this.setState({scanning: false});
  this.setState({scanText: "Scan Devices"});
  this.setState({readValue: "Scan Finished"});
}

eventDeviceFound(e){
  this.setState(previousState => ({
    data: [...previousState.data, e]
  }));
}

eventBtDeviceConnecting(e){
  this.setState({readValue: 'Connecting...'})
}

eventBtDeviceDisconnected(e){
  this.setState({readValue: 'Disconnected'})
}

eventBtDeviceConnected(e){
  this.setState({readValue: 'Connected'})
}

eventDataReceived(e){
  let type = e.type;
  let batch = e.batch;
  let intNo = e.intNo;
  let result = e.result;
  let year = e.year;
  this.setState({readValue:result});
}

eventBtDeviceNotFound(e){}

toggleScan = () => {
  if(this.state.scanning){
    this.setState({scanning: false});
    this.setState({scanText: "Scan Devices"});
    Dt157Module.stopScanning();
  } else{
    this.setState({scanning: true});
    this.setState({scanText: "Stop Scanning"});
    this.setState({data: []});
    Dt157Module.startScanning();
  }
}

onDeviceSelected = (device) => {
  Dt157Module.connectDevice(device.address);
  this.setState({readValue: 'Connecting...'})
}

renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };

render() {
  return (
    <View style={{flex: 1}}>
      <View style={{flex: 2, backgroundColor: 'powderblue', alignItems:'center', justifyContent:'center'}}>
          <Text style={styles.sensorValue}>{this.state.readValue}</Text>
      </View>

      <View style={{flex: 10, backgroundColor:'white'}}>
        <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
          <FlatList
             ItemSeparatorComponent={this.renderSeparator}
            data={this.state.data}
              renderItem={({ item }) => (
                <ListItem
                    title={item.name}
                    subtitle={item.address}
                    onPress={() => this.onDeviceSelected(item)}
                    containerStyle={{ borderBottomWidth: 0 }}
                />)}
              />
              </List>
      </View>
        <View style={{flex: 1, backgroundColor: 'skyblue',  justifyContent:'center'}}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.toggleScan} >
              <Text style={{color:'white'}}>{this.state.scanText}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sensorValue:{
    fontFamily: "LED.Font",
    fontSize: 30,
    textAlign: "center",
    justifyContent: 'center'
  },
  button: {
    alignItems: 'center',
    padding: 10
  }
})
