# Dt157 Native Android Module

## Metodlar
### startScanning()
Bluetooth cihaz taraması başlatır.
### stopScanning()
Cihaz taramasını sonlandırır.
### connectDevice(address)
Adresi girilen cihaza bağlanır.

## Events
**MESSAGE_BT_NOT_APPLICABLE**

Uygulamanın çalışacağı cihaz hem android sürümü olarak (en az API 18) hem de donanımsal olarak BLE(Bluetooth Low Energy) desteklemesi lazım. Donanımsal olarak desteklemiyorsa bu event gönderilir.

**MESSAGE_BT_NOT_READY**

 - Cihazlar taranmak istediğinde bluetooth kapalıysa
 - Listelenen cihazlardan herhangi birisi seçilirken bluetooth kapalıysa
 - Bağlantı kurulduktan sonra bluetooth kapatılırsa

Bu event gönderilecek.

**MESSAGE_SCAN_STARTED**

Tarama işlemi başlarken bu event gönderilecek. `Dt157Module.startScanning();` metodu Javascript den çağrıldığında, android tarafından taramanın gerçekten çalıştığını belirtmek için bu event gönderiliyor.

**MESSAGE_SCAN_STOPPED**

Tarama işlemi bittiğinde bu event gönderilecek. Tarama işlemi iki türlü bitebilir. 

 - Tarama aktif olarak devam ederken kullanıcı aniden kendi isteğiyle `Dt157Module.stopScanning();` metodunu çağırıp. 
 - İkincisi ise, tarama işleminin doğal yolla bitmesiyle(Tarama işlemi 10 saniye sürüyor, düzenlenebilir)

**MESSAGE_DEVICE**

Tarama esnasında bluetooth cihazlar bulundukça, onlar bu event’le javascript tarafına gönderilir.

    {address : ‘address’,  name: ‘name’}

**MESSAGE_DEVICE_DISCONNECTED**

Bağlantı koptuğunda gönderilir.

**MESSAGE_DATA**

En basit anlamıyla, cihazda data okunduğunda bu event'le JSON olarak gönderilir.

    eventDataReceived(e){
      let type = e.type;
      let batch = e.batch;
      let intNo = e.intNo;
      let result = e.result;
      let year = e.year;
      this.setState({readValue:result});
    }

Ekrandaki kalınlık değeri e.result ile alınır.

**MESSAGE_BT_DEVICE_NOT_FOUND**

Tarama işlemiyle listelenmiş cihazlardan herhangi birisi seçilmek istenip, kullanım dışı olduğunda (cihazın kapatılması ve kapsam alanından çıkması) bu event gönderilir.

**MESSAGE_BT_DEVICE_CONNECTED**

Tarama işlemiyle listelenmiş cihazlardan, seçim yapılıp bağlantı kurulduğunda bu event gönderilir.

**MESSAGE_BT_READY**

Bluetooth aktif hale getirilince bu event gönderilir.

**MESSAGE_LOCATION_NOT_READY**

Tarama yapılmadan önce, uygulamanın konum yetkisi yoksa bu event gönderilir.

**MESSAGE_LOCATION_READY**

Konum izini verildikten sonra bu event gönderilir.

**MESSAGE_BT_DEVICE_CONNECTING**

Cihaza bağlanırken bu event gönderilir.