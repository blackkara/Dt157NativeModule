package com.dt157nativemodule;

/**
 * Created by blackkara on 3/17/2018.
 */

public enum  DataObjMode {
    ICTTAddressObjMode,
    ICTTDataLoggerEndObjMode,
    ICTTDataLoggerObjMode,
    ICTTObjMode,
    None;

    static {
        DataObjMode[] var0 = new DataObjMode[]{None, ICTTObjMode, ICTTAddressObjMode, ICTTDataLoggerObjMode, ICTTDataLoggerEndObjMode};
    }
}