package com.dt157nativemodule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by blackkara on 3/17/2018.
 */

public class Dt157ByteProcessor {

    private final Listener mListener;

    public interface Listener{
        void onDt157DataReady(ICTTObj value);

    }

    public Dt157ByteProcessor(Listener listener){
        mListener = listener;
    }

    private static final Byte START_BYTE = -86;

    private List<Byte> mData = new ArrayList<>();
    private int mDataLength = 11;
    private int mSecondCode;
    private int mSixCode;

    public void handleBytes(Byte[] bytes){
        mData.addAll(Arrays.asList(bytes));

        if(mData.get(0) == null){
            mData.clear();
        } else {
            process();
        }
    }

    private void process() {
        if(mData.size() >= 6){
            if(mData.get(0) == START_BYTE){
                int length = (int) mData.get(1);
                if(length == 11){
                    mDataLength = 11;
                } else if(length == 9){
                    mDataLength = 9;
                } else {
                    mDataLength = length;
                }

                if(mDataLength <= 0){
                    mData.clear();
                    return;
                }

                if(mData.size() >= mDataLength){
                    List<Byte> copied = copy(mDataLength);
                    process();
                    processData(copied);
                    mData.clear();
                }
            } else {
                while (mData.size() > 0){
                    if(mData.get(0) == START_BYTE){
                        return;
                    }
                    mData.remove(0);
                }
            }
        }
    }

    private void processData(List<Byte> bytes) {
        mSecondCode = Dt157Util.getShort(bytes.get(1));
        mSixCode = Dt157Util.getShort(bytes.get(6));

        ICTTObj data = null;

        if(mSecondCode == 11 && mSixCode == 0){
            data = new ICTTObj(convertToByteArray(bytes));
        }

        mListener.onDt157DataReady(data);
    }

    private List<Byte> copy(int mDataLength) {
        List<Byte> returnList = new ArrayList<>();
        List<Byte> newList = new ArrayList<>();

        for(int i = 0; i < mData.size(); i++){
            if(i <= mDataLength - 1){
                returnList.add(mData.get(i));
            } else {
                newList.add(mData.get(i));
            }
        }

        mData = newList;
        return returnList;
    }

    private byte[] convertToByteArray(List<Byte> bytes){
        byte[] byteArr = new byte[bytes.size()];
        for(int i = 0; i<bytes.size(); i++){
            byteArr[i] = bytes.get(i);
        }
        return byteArr;
    }
}
