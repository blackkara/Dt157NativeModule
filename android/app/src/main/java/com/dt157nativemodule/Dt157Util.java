package com.dt157nativemodule;

/**
 * Created by blackkara on 3/17/2018.
 */

public class Dt157Util {
    public static String dec_hex(byte[] var0) {
        String var1;
        if(var0 != null && var0.length > 0) {
            var1 = "";

            for (byte aVar0 : var0) {
                String var3 = Integer.toHexString(255 & aVar0);
                if (var3.length() == 1) {
                    var3 = '0' + var3;
                }

                var1 = var1 + var3.toUpperCase();
            }
        } else {
            var1 = null;
        }

        return var1;
    }

    public  static int getShort(byte var1) {
        return var1 & 255;
    }

    public static Byte[] toObjects(byte[] bytesPrim) {

        Byte[] bytes = new Byte[bytesPrim.length];
        int i = 0;
        for (byte b : bytesPrim) bytes[i++] = b; //Autoboxing
        return bytes;

    }
}
