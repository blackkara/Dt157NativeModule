package com.dt157nativemodule;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.facebook.react.bridge.UiThreadUtil.runOnUiThread;

/**
 * Created by blackkara on 3/3/2018.
 *
 */
public class Dt157Module extends ReactContextBaseJavaModule implements LifecycleEventListener, ActivityEventListener, Dt157ByteProcessor.Listener{
    private static final String RESPONSE_IS_ENABLED = "RESPONSE_IS_ENABLED";
    private static final String RESPONSE_HAS_BLUETOOTH = "RESPONSE_HAS_BLUETOOTH";
    private static final String REQUESTED_BLUETOOTH = "REQUESTED_BLUETOOTH";

    private static final String ERROR_NO_ACTIVITY = "ERROR_NO_ACTIVITY";
    private static final String TAG = Dt157Module.class.getSimpleName();

    private static final int REQUEST_ENABLE_BT = 1000;
    private static final int REQUEST_LOCATION = 1001;

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;

    private Promise mPromise;
    private Dt157ByteProcessor mByteProcessor;


    public Dt157Module(ReactApplicationContext reactContext) {
        super(reactContext);
        mBluetoothManager = (BluetoothManager) getReactApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        mByteProcessor = new Dt157ByteProcessor(this);

        reactContext.addActivityEventListener(this);
        reactContext.addLifecycleEventListener(this);
    }

    @Override
    public String getName() {
        return "Dt157Module";
    }

    @ReactMethod
    public void hasBluetooth(Promise promise){
        WritableMap result = Arguments.createMap();
        result.putBoolean(RESPONSE_HAS_BLUETOOTH, hasBluetoothCapability());
        promise.resolve(result);
    }


    private boolean hasBluetoothCapability(){
        return getReactApplicationContext()
                .getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    @ReactMethod
    public void isBluetoothEnabled(Promise promise){
        WritableMap result = Arguments.createMap();
        result.putBoolean(RESPONSE_IS_ENABLED, mBluetoothAdapter.isEnabled());
        promise.resolve(result);
    }

    @ReactMethod
    public void enableBluetooth(Promise promise){
        Activity activity = getCurrentActivity();

        if(activity == null){
            promise.reject(new Throwable(ERROR_NO_ACTIVITY));
            return;
        }

        if(mBluetoothAdapter.isEnabled()){
            WritableMap result = Arguments.createMap();
            result.putBoolean(RESPONSE_IS_ENABLED, mBluetoothAdapter.isEnabled());
            promise.resolve(result);

            return;
        } else {
            requestBluetoothEnable();
            WritableMap result = Arguments.createMap();
            result.putBoolean(REQUESTED_BLUETOOTH, true);
            promise.resolve(result);
        }

        mPromise = promise;
    }

    private void enableBluetooth(){
        if(mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()){
            requestBluetoothEnable();
        }
    }


    private void requestBluetoothEnable(){
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        getCurrentActivity().startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        Log.d(TAG, "Requested user enables Bluetooth. Try starting the scan again.");
    }


    private boolean hasLocationPermissions() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                getReactApplicationContext()
                        .checkSelfPermission(
                                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestLocationPermission() {
        Activity activity = getCurrentActivity();
        if(activity != null){
            activity.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION);
        }
    }


    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_ENABLE_BT){
            if(resultCode == RESULT_OK){
                sendEvent(getReactApplicationContext(), MESSAGE_BT_READY, Arguments.createMap());
            } else {
                sendEvent(getReactApplicationContext(), MESSAGE_BT_NOT_READY, Arguments.createMap());
            }
        } else if(requestCode == REQUEST_LOCATION){
            if(resultCode == RESULT_OK){
                sendEvent(getReactApplicationContext(), MESSAGE_LOCATION_READY, Arguments.createMap());
            } else {
                sendEvent(getReactApplicationContext(), MESSAGE_LOCATION_NOT_READY, Arguments.createMap());
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {

    }


    @ReactMethod
    public void startScanning(){
        if(isBluetoothReady()){
            if(hasLocationPermissions()){
                scanLeDevice(true);
                sendEvent(getReactApplicationContext(), MESSAGE_SCAN_STARTED, Arguments.createMap());
            } else {
                sendEvent(getReactApplicationContext(), MESSAGE_LOCATION_NOT_READY, Arguments.createMap());
                requestLocationPermission();
            }
        } else {
            requestBluetoothEnable();
            sendEvent(getReactApplicationContext(), MESSAGE_BT_NOT_READY, Arguments.createMap());
        }
    }

    @ReactMethod
    public void stopScanning(){
        if(isBluetoothReady()){
            scanLeDevice(false);
            sendEvent(getReactApplicationContext(), MESSAGE_SCAN_STOPPED, Arguments.createMap());
        } else {
            sendEvent(getReactApplicationContext(), MESSAGE_BT_NOT_READY, Arguments.createMap());
        }
    }

    @ReactMethod
    public void connectDevice(String address){
        if(isBluetoothReady()){
            BluetoothDevice device = getDevice(address);
            if(device != null){
                sendEvent(getReactApplicationContext(), MESSAGE_BT_DEVICE_CONNECTING, Arguments.createMap());
                mBluetoothGatt = device.connectGatt(getReactApplicationContext(), false, mGattCallback);
            } else {
                sendEvent(getReactApplicationContext(), MESSAGE_BT_DEVICE_NOT_FOUND, Arguments.createMap());
            }
        } else {
            sendEvent(getReactApplicationContext(), MESSAGE_BT_NOT_READY, Arguments.createMap());
        }
    }

    private List<BluetoothDevice> mSharedDevices = new ArrayList<>();
    private BluetoothDevice getDevice(String address){
        for(BluetoothDevice device : mSharedDevices){
            if(device.getAddress().equals(address)){
                return device;
            }
        }
        return null;
    }

    private static final String MESSAGE_BT_NOT_READY = "MESSAGE_BT_NOT_READY";
    private static final String MESSAGE_SCAN_STOPPED = "MESSAGE_SCAN_STOPPED";
    private static final String MESSAGE_SCAN_STARTED = "MESSAGE_SCAN_STARTED";
    private static final String MESSAGE_DEVICE = "MESSAGE_DEVICE";
    private static final String MESSAGE_BT_DEVICE_CONNECTED = "MESSAGE_BT_DEVICE_CONNECTED";
    private static final String MESSAGE_DEVICE_DISCONNECTED = "MESSAGE_DEVICE_DISCONNECTED";
    private static final String MESSAGE_BT_DEVICE_NOT_FOUND = "MESSAGE_BT_DEVICE_NOT_FOUND";
    private static final String MESSAGE_DATA = "MESSAGE_DATA";
    private static final String MESSAGE_BT_READY = "MESSAGE_BT_READY";
    private static final String MESSAGE_LOCATION_READY = "MESSAGE_LOCATION_READY";
    private static final String MESSAGE_LOCATION_NOT_READY = "MESSAGE_LOCATION_NOT_READY";
    private static final String MESSAGE_BT_DEVICE_CONNECTING = "MESSAGE_BT_DEVICE_CONNECTING";


    private static final String PARAM_DEVICE_ADDRESS = "address";
    private static final String PARAM_DEVICE_NAME = "name";

    private boolean mScanning;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private static final long SCAN_PERIOD = 10000;
    private void scanLeDevice(final boolean enable) {
        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    sendEvent(getReactApplicationContext(), MESSAGE_SCAN_STOPPED, Arguments.createMap());
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mSharedDevices = new ArrayList<>();
            mConnectedDeviceUUID = null;
            if(mBluetoothGatt != null){
                mBluetoothGatt.close();
                mBluetoothGatt = null;
            }
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!mSharedDevices.contains(device)){
                        mSharedDevices.add(device);
                        WritableMap result = Arguments.createMap();
                        result.putString(PARAM_DEVICE_ADDRESS, device.getAddress());
                        result.putString(PARAM_DEVICE_NAME, device.getName() == null ? "Unknown Device" : device.getName());
                        sendEvent(getReactApplicationContext(), MESSAGE_DEVICE, result);
                    }
                }
            });
        }
    };

    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                gatt.discoverServices();
                mConnectedDeviceUUID = gatt.getDevice().getAddress();
                sendEvent(getReactApplicationContext(), MESSAGE_BT_DEVICE_CONNECTED, Arguments.createMap());
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                sendEvent(getReactApplicationContext(), MESSAGE_DEVICE_DISCONNECTED, Arguments.createMap());
                mConnectedDeviceUUID = null;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            for(BluetoothGattService service : gatt.getServices()){
                for(BluetoothGattCharacteristic characteristic : service.getCharacteristics()){
                    if(characteristic.getProperties() == 16){
                        gatt.setCharacteristicNotification(characteristic, true);
                        for (BluetoothGattDescriptor descriptor : characteristic.getDescriptors()){
                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                            gatt.writeDescriptor(descriptor);
                        }
                    }
                }
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            mByteProcessor.handleBytes(Dt157Util.toObjects(characteristic.getValue()));
        }
    };

    private void sendEvent(ReactContext reactContext, String eventName, @Nullable WritableMap params) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, params);
    }

    private boolean isBluetoothReady(){
        return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
    }

    private static final String PARAM_DATA_TYPE = "type";
    private static final String PARAM_DATA_BATCH = "batch";
    private static final String PARAM_DATA_INT_NO = "intNo";
    private static final String PARAM_DATA_RESULT = "result";
    private static final String PARAM_DATA_YEAR = "year";

    @Override
    public void onDt157DataReady(ICTTObj value) {
        Map<String, Object> result = new HashMap<>();
        result.put(PARAM_DATA_TYPE, value.getType());
        result.put(PARAM_DATA_BATCH, value.getBatch());
        result.put(PARAM_DATA_INT_NO, value.getIntNo());
        result.put(PARAM_DATA_RESULT, value.getResult());
        result.put(PARAM_DATA_YEAR, value.getYear());
        sendEvent(getReactApplicationContext(), MESSAGE_DATA, Arguments.makeNativeMap(result));
    }

    private String mConnectedDeviceUUID;
    private BluetoothGatt mBluetoothGatt;

    @Override
    public void onHostResume() {
        if(mConnectedDeviceUUID != null){
            connectDevice(mConnectedDeviceUUID);
        }
    }

    @Override
    public void onHostPause() {
        if(mBluetoothGatt != null){
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }
    }

    @Override
    public void onHostDestroy() {
        if(mBluetoothGatt != null){
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }
    }
}

